# GIT

### key_pair

voce pode usar uma key_pair existente na Cloud, usar a chave ssh do seu diretorio padrão, ou criar nova chave para usar. Abaixo segue como criar nova chave ssh:  

```
crie sua chave SSH:
# mkdir .ssh
# ssh-keygen -t rsa  -f .ssh/valida
responda com <enter>
```
### aqui vamos validar:

[ ] Uso do terraform para subir infra e para criar as instancias  
[ ] Uso do Packer para criar a imegem ideal  
[ ] Uso do Terraform para criar instancias, usando a imagem do packer  
[ ] Atualização desta instancia via ansible  
[ ] Subir container em instancias EC2  
[ ] Subir container usando ECR  
