variable "region" {
  default = "us-east-2"
}
variable "availabilityZonePublic1" {
  default = "us-east-2a"
}
variable "availabilityZonePublic2" {
  default = "us-east-2b"
}
variable "availabilityZonePrivate1" {
  default = "us-east-2c"
}
variable "availabilityZonePrivate2" {
  default = "us-east-2d"
}

# VPC
variable "instanceTenancy" {
  default = "default"
}
variable "dnsSupport" {
  default = true
}
variable "dnsHostNames" {
  default = true
}
variable "vpcCIDRblock" {
  default = "10.0.0.0/16"
}


# subnet
variable "publicsCIDRblock1" {
  default = "10.0.1.0/24"
}
variable "publicsCIDRblock2" {
  default = "10.0.2.0/24"
}
variable "privatesCIDRblock1" {
  default = "10.0.3.0/24"
}
variable "privatesCIDRblock2" {
  default = "10.0.4.0/24"
}
variable "publicdestCIDRblock" {
  default = "0.0.0.0/0"
}
variable "localdestCIDRblock" {
  default = "10.0.0.0/16"
}
variable "ingressCIDRblock" {
  type    = list(any)
  default = ["0.0.0.0/0"]
}
variable "egressCIDRblock" {
  type    = list(any)
  default = ["0.0.0.0/0"]
}
variable "mapPublicIP" {
  default = true
}
variable "mapPrivateIP" {
  default = false
}


variable "tag_name" {
  default = "valida"
}
variable "tag_projeto" {
  default = "valida"
}


variable "bucket_backend" {
  default = "terraform-backend"
  
}

variable "instance_count" {
  default = "2"
}

variable "instance_type" {
  default = "t2.nano"
}