# usando chave ssh criada ou a existente que fica no diretorio ~/.ssh
resource "aws_key_pair" "key-valida" {
  key_name   = "key-valida"
  public_key = file(".ssh/valida.pub")
}

