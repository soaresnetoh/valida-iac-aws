data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

# data "aws_sg" "sg" {
#   id = aws_security_group.sg_valida.id
# }

resource "aws_instance" "terraform" {
  count         = var.instance_count

  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  key_name      = aws_key_pair.key-valida.key_name

  # vpc_security_group_ids = aws_security_group.sg_valida.id
  # vpc_security_group_ids = aws_security_group.sg_valida.id
  # vpc_security_group_ids = ["${aws_security_group.sg_valida.*.id}"]
  # vpc_security_group_ids = data.aws_sg.sg.id

  # ["${aws_security_group.sg_valida.id}"]
  tags = {
    Name    = "Terraform-${count.index + 1}"
    projeto = var.tag_projeto
  }
}
