resource "aws_vpc" "VPC_valida" {
  cidr_block           = var.vpcCIDRblock
  instance_tenancy     = var.instanceTenancy
  enable_dns_support   = var.dnsSupport
  enable_dns_hostnames = var.dnsHostNames
  tags = {
    Name    = var.tag_name
    projeto = var.tag_projeto
  }
}
resource "aws_subnet" "valida_pub1" {
  vpc_id                  = aws_vpc.VPC_valida.id
  cidr_block              = var.publicsCIDRblock1
  map_public_ip_on_launch = var.mapPublicIP
  availability_zone       = var.availabilityZonePublic1
  tags = {
    Name    = "Public subnet 1"
    projeto = var.tag_projeto
  }
}
resource "aws_subnet" "valida_pri1" {
  vpc_id                  = aws_vpc.VPC_valida.id
  cidr_block              = var.privatesCIDRblock1
  map_public_ip_on_launch = var.mapPrivateIP
  availability_zone       = var.availabilityZonePrivate1
  tags = {
    Name    = "Private subnet 1"
    projeto = var.tag_projeto
  }
}
resource "aws_network_acl" "Public_NACL" {
  vpc_id     = aws_vpc.VPC_valida.id
  subnet_ids = [aws_subnet.valida_pub1.id]
  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = var.publicdestCIDRblock
    from_port  = 22
    to_port    = 22
  }
  egress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = var.publicdestCIDRblock
    from_port  = 22
    to_port    = 22
  }
  tags = {
    Name    = "Public NACL"
    projeto = var.tag_projeto
  }
}
resource "aws_internet_gateway" "IGW_valida" {
  vpc_id = aws_vpc.VPC_valida.id
  tags = {
    Name    = "Internet gateway valida"
    projeto = var.tag_projeto
  }
}
resource "aws_route_table" "Public_RT_valida" {
  vpc_id = aws_vpc.VPC_valida.id
  tags = {
    Name    = "Public Route table"
    projeto = var.tag_projeto
  }
}
resource "aws_route" "internet_access_valida" {
  route_table_id         = aws_route_table.Public_RT_valida.id
  destination_cidr_block = var.publicdestCIDRblock
  gateway_id             = aws_internet_gateway.IGW_valida.id
}
resource "aws_route_table_association" "Public_association_1" {
  subnet_id      = aws_subnet.valida_pub1.id
  route_table_id = aws_route_table.Public_RT_valida.id
}
