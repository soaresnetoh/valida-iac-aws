resource "random_id" "this" {
  byte_length = 1
}

resource "random_string" "this" {
  length  = 6
  special = false
  upper   = false
}
resource "aws_s3_bucket" "bucket-backend" {
    # bucket = "bucket-backend-s3"
    # bucket = "${var.bucket_backend}-${random_id.this.id}"
    bucket = "${var.bucket_backend}-${random_string.this.id}"
    lifecycle {
        prevent_destroy = false
    }
    tags = {
        Name = "backend-S3"
        projeto = var.tag_projeto
    }
}

resource "aws_s3_bucket_acl" "bucket-backend" {
  bucket = aws_s3_bucket.bucket-backend.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "bucket-backend" {
  bucket = aws_s3_bucket.bucket-backend.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_dynamodb_table" "terraform-locks" {
    name         = "terraform-locks-mohit-20200929"
    billing_mode = "PAY_PER_REQUEST"
    hash_key     = "LockID"

    attribute {
        name = "LockID"
        type = "S"
    }
}