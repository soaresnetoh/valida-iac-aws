terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
  # backend "s3" {
  #   bucket = "terrafrom-state-20200929"
  #   key = "validar-snapshot-aws-devops/terraform.tfstate"
  #   region = "us-east-2"
  #   dynamodb_table = "terraformlocks-mohit-202009292250"
  #   encrypt = true
  # }
}

provider "aws" {
  region                  = var.region
  # shared_credentials_file = "~/.aws/credentials"
  profile                 = "hernanitotal"
}