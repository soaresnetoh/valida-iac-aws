output "public_ip" {
  description = "public IP of instance"
  value       = join(" | ", aws_instance.terraform.*.public_ip)
}

output "private_ip" {
  description = "Private IP of instance"
  value       = join(" | ", aws_instance.terraform.*.private_ip)
}

output "public_dns" {
  description = "Public DNS of instance (or DNS of EIP)"
  value       = join(" | ", aws_instance.terraform.*.public_dns)
}

output "id" {
  description = "Disambiguated ID of the instance"
  value       = join(" | ", aws_instance.terraform.*.id)
}

output "arn" {
  description = "ARN of the instance"
  value       = join(" | ", aws_instance.terraform.*.arn)
}

output "bucket-backend" {
  description = "bucket-backend"
  value       = aws_s3_bucket.bucket-backend.id
}


output "tags_all" {
  description = "A map of tags assigned to the resource, including those inherited from the provider default_tags configuration block"
#   value       = try(aws_instance.terraform[0].tags_all, aws_spot_instance_request.terraform[0].tags_all, {})
#   value = join(" | ", "${lookup(aws_instance.terraform[0].tags_all, "Name")}")
#   value = "${lookup(aws_instance.terraform[0].tags_all, "Name")}"
  value = aws_instance.terraform.*.tags_all
}

# output "name" {
#   description = "Instance name"
#   value       = join(" | ", aws_instance.terraform.*.name)
# }

# output "ssh_key_pair" {
#   description = "Name of the SSH key pair provisioned on the instance"
#   value       = var.ssh_key_pair
# }